=== Classic Cosmetics ===
Contributors: classictemplate
Tags: blog, e-commerce, photography, one-column, two-columns, three-columns, four-columns, grid-layout, right-sidebar, left-sidebar, custom-logo, post-formats, translation-ready, full-width-template, footer-widgets, featured-images, custom-colors, editor-style, wide-blocks, block-styles, custom-header, custom-background, custom-menu, sticky-post, threaded-comments, theme-options, rtl-language-support
Requires at least: 5.0
Tested up to: 6.5
Requires PHP: 5.6
Stable tag: 0.4
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Classic Cosmetics is a versatile WordPress theme designed for anyone in the beauty industry, from makeup artists to cosmetic brands and beauty bloggers. With its timeless design and user-friendly features, it's perfect for showcasing products, sharing beauty tips, and connecting with customers. This theme has a clean and elegant look, with stylish typography and customizable color options to match your brand's aesthetic. 

== Description ==

Classic Cosmetics is a versatile WordPress theme designed for anyone in the beauty industry, from makeup artists to cosmetic brands and beauty bloggers. With its timeless design and user-friendly features, it's perfect for showcasing products, sharing beauty tips, and connecting with customers. This theme has a clean and elegant look, with stylish typography and customizable color options to match your brand's aesthetic. Whether you're a small beauty business or a large cosmetics company, the Classic Cosmetics WordPress Theme offers a professional and polished platform to showcase your offerings. With this theme, you can easily create pages to display your products, from makeup collections to skincare lines, with stunning images and detailed descriptions. You can also integrate a blog section to share beauty tutorials, product reviews, and industry insights, helping to engage your audience and establish your expertise in the field. One of the benefits of the Classic Cosmetics WordPress Theme is its ease of use. You don't need to be a tech wizard to set up and customize your website – the theme comes with intuitive tools and a user-friendly interface that make it easy to create a professional-looking site in no time. Another advantage is its responsiveness, meaning your website will look great and function smoothly on all devices, from desktop computers to smartphones and tablets. This ensures that your customers can access your site wherever they are, increasing your reach and potential for sales.

== Changelog ==

= 0.1 =
* Initial Version Released.

= 0.2 =
* Added get started.
* Added about theme links in customizer.
* Added footer link.
* Added language folder.

= 0.3 =
* Changed the urls for the theme.
* Removed menu toggle condition to preview menus.
* Done css for about theme link in customizer.
* Done theme info css.
* Added go pro link in slider, team and footer section.
* Updated pot file.

= 0.4 =

* Added default image for slider
* Added default size for slider in customizer.
* Added css for slider for slider height.
* Added latest bootstrap version.
* Added css in media for container width.
* Added css for shop page product in media.
* Done homepage css.

== Resources ==

Classic Cosmetics WordPress Theme, Copyright 2024 classictemplate
Classic Cosmetics is distributed under the terms of the GNU GPL.

Classic Cosmetics WordPress Theme is derived from Businessweb Plus WordPress Theme, Copyright 2016 GraceThemes(gracethemes.com)
Businessweb Plus WordPress Theme is distributed under the terms of the GNU GPL

Classic Cosmetics WordPress Theme incorporates code from Twenty Seventeen WordPress Theme, Copyright 2020 Twenty Seventeen WordPress Theme is distributed under the terms of the GNU GPL

Bootstrap
Bootstrap v5.3.3 (https://getbootstrap.com/)
Copyright 2011-2024 The Bootstrap Authors
https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-dist.zip
Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)

Owl Carousel
David Deutsch
Copyright 2013-2017 David Deutsch
https://github.com/OwlCarousel2/OwlCarousel2
License: The MIT License (MIT)
https://github.com/OwlCarousel2/OwlCarousel2/blob/develop/LICENSE

Font-Awesome:
Font Awesome 4.3.0 by @davegandy - http://fontawesome.io - @fontawesome
License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

TGMPA
GaryJones Copyright (C) 1989, 1991
https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/LICENSE.md
License: GNU General Public License v2.0

Pxhere Images,
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/license

Slider Image
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/1038451

Product Image
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/660362

Product Image
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/1439127

Product Image
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/1638924

Product Image
License: CC0 1.0 Universal (CC0 1.0)
Source: https://pxhere.com/en/photo/794256

== Homepage Installation ==

Step-1. Go to Dashboard -- Pages -- Add new page -- Add page attribute (Home Page) -- Publish it.
Step-2. Go to Dashboard -- Settings -- Reading -- Click on static page -- Select home page -- Save changes.

Header section
Step-1. Go to Dashboard -- Appearnace -- Customize -- Theme option panel -- Header section -- Add Offer Text -- Publish it.

Social media section
Step-1. Go to Dashboard -- Appearnace -- Customize -- Theme option panel -- Social media section -- Add Social media links -- Publish it.

Slider section
Step-1. Go to Dashboard -- Post -- Add new category -- Publish it.
Step-2. Go to Dashboard -- Post -- Add new post -- Add content and featured image -- Publish it.
Step-3. Go to Dashboard -- Appearnace -- Customize -- Theme option panel -- Mange Slider -- Select the category -- Publish it.

Product section
Step-1. Active and install Woocommerce Plugin
Step-2. Go to Dashborad -- Appearnace -- Product -- Catergories -- Create Product Categories -- Publish it.
Step-3. Go to Dashboard -- Appearnace -- Customize -- Theme option panel -- Manage Trending Product Section
 -- Select the category-- Publish it.

Footer section
Step-1. Go to Dashboard -- Appearnace -- Customize -- Theme option panel -- Footer section -- Add copyright text -- Publish it.