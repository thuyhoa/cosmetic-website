<?php

  $classic_cosmetics_color_scheme_css = '';

  //---------------------------------Logo-Max-height--------- 
  $classic_cosmetics_logo_width = get_theme_mod('classic_cosmetics_logo_width');

  if($classic_cosmetics_logo_width != false){

    $classic_cosmetics_color_scheme_css .='.logo .custom-logo-link img{';

      $classic_cosmetics_color_scheme_css .='width: '.esc_html($classic_cosmetics_logo_width).'px;';

    $classic_cosmetics_color_scheme_css .='}';
  }
