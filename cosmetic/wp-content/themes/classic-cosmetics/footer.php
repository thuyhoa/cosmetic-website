<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Classic Cosmetics
 */
?>
<div id="footer">
  <?php 
    $classic_cosmetics_footer_widget_enabled = get_theme_mod('classic_cosmetics_footer_widget', false);
    
    if ($classic_cosmetics_footer_widget_enabled !== false && $classic_cosmetics_footer_widget_enabled !== '') { ?>

    <div class="footer-widget">
    	<div class="container">
        <?php if ( ! dynamic_sidebar( 'footer-1' ) ) : ?>
        <?php endif; // end footer widget area ?>
                  
        <?php if ( ! dynamic_sidebar( 'footer-2' ) ) : ?>
        <?php endif; // end footer widget area ?>
      
        <?php if ( ! dynamic_sidebar( 'footer-3' ) ) : ?>
        <?php endif; // end footer widget area ?>
        
        <?php if ( ! dynamic_sidebar( 'footer-4' ) ) : ?>
        <?php endif; // end footer widget area ?>      
        <div class="clear"></div>
      </div>
    </div>
  <?php } ?>
  <div class="clear"></div> 

  <div class="copywrap">
  	<div class="container">
      <p><a href="<?php echo esc_html(get_theme_mod('classic_cosmetics_copyright_link',__('https://www.theclassictemplates.com/products/free-cosmetic-wordpress-theme/','classic-cosmetics'))); ?>" target="_blank"><?php echo esc_html(get_theme_mod('classic_cosmetics_copyright_line',__('Cosmetics WordPress Theme','classic-cosmetics'))); ?></a> <?php echo esc_html('By Classic Templates','classic-cosmetics'); ?></p>
    </div>
  </div>
</div>

<?php if(get_theme_mod('classic_cosmetics_scroll_hide',false)){ ?>
 <a id="button"><?php esc_html_e('TOP', 'classic-cosmetics'); ?></a>
<?php } ?>

<?php wp_footer(); ?>
</body>
</html>